package com.iisight.iisightdemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.elevensight.sdk.sdk.IISightSDKManager;
import com.iisight.iisightdemo.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onSignOutClicked(View view) {
        IISightSDKManager.getInstance().logoutUser(new IISightSDKManager.ICallback() {
            @Override
            public void process(Object o) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        }, new IISightSDKManager.ICallback() {
            @Override
            public void process(Object o) {
            }
        });
    }

    public void onMakeCallClicked(View view) {
        IISightSDKManager.getInstance().makeCall("", getApplicationContext());
    }
}
